suceso(X) :- prestar(X).
suceso(X) :- enfadar(X).


prestar(prest_1).
prestar(prest_2).
prest_1(coche1,juan,maria).
prest_2(coche1,maria,pablo).

enfadar(enf1).
enf_1(juan,maria,prest_2).


persona(juan).
persona(maria).
persona(pablo).

propietario(coche1,juan).
marca(coche1,seat).

es_vehiculo(X) :- marca(X,seat).
tiene_sistema_electrico(X) :- es_vehiculo(X).
tiene_bateria(X) :- tiene_sistema_electrico(X).
tiene_acido(X) :- tiene_bateria(X).
tiene_quimico(X) :- tiene_acido(X).

coche_de_es_vehiculo(X) :- propietario(Y,X), es_vehiculo(Y).
coche_de_tiene_sistema_electrico(X) :- propietario(Y,X), tiene_sistema_electrico(Y).
coche_de_tiene_bateria(X) :- propietario(Y,X), tiene_bateria(Y).
coche_de_tiene_quimico(X) :- propietario(Y,X), tiene_quimico(Y).
