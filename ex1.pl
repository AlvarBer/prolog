es_un(elefante_circense, elefante).
es_un(elefante, animal).
es_un(elefante_circense, acrobata).
es_un(disfraz, ropa).
es(X,Y) :- es_un(X,Z), es_un(Z,Y).

tiene_parte(elefante, cabeza).
tiene_parte(elefante, trompa).
tiene_parte(cabeza, boca).
tiene_parte(animal, corazon).
tiene_parte(acrobata, disfraz).
tiene(X,Y) :- tiene_parte(Z,Y), es(X,Z).
